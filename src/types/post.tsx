export interface PostRequest {
  userId: number;
  title: string;
  body: string;
}

export interface PostResponse {
  id: number;
  userId: number;
  title: string;
  body: string;
}

export interface GetsResponse extends Array<PostResponse> { }

export interface GetResponse extends PostResponse { }
import { useState } from 'react';
import { styled } from '@mui/material/styles';
import { Card, CardHeader, CardContent, CardActions, Collapse, Avatar, IconButton, IconButtonProps, Typography } from '@mui/material';
import { red } from '@mui/material/colors';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import { PostResponse, GetsResponse } from './types/post';
import { deletePost } from './api/restfulApi';
import { useMutation } from "react-query";


interface ExpandMoreProps extends IconButtonProps {
  expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
  const { expand, ...other } = props;
  return <IconButton {...other} />;
})(({ theme, expand }) => ({
  transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
  marginLeft: 'auto',
  transition: theme.transitions.create('transform', {
    duration: theme.transitions.duration.shortest,
  }),
}));

export default function PostCard({ post, onSetPostId: setPostId, onOpenDialog: openDialog, listPost, onSetListPost }: { post: PostResponse, onSetPostId: Function, onOpenDialog: Function, listPost: GetsResponse, onSetListPost: Function }) {
  const [expanded, setExpanded] = useState(false);

  const { mutate: handleDeletePost } = useMutation((id: number) => deletePost(id), {
    onSuccess(res, id: number) {
      alert("Success delete post");
      console.log("Success delete post");
      console.log(res);


      const foundIndex = listPost.findIndex(obj => obj.id === id);
      console.log(foundIndex);
      const tmpList = [...listPost];
      if (foundIndex >= 0) {
        tmpList.splice(foundIndex, 1);
        onSetListPost(tmpList);
      }
      else {
        console.log("Exception: not found post to delete")
      }
    },
    onError(error: any) {
      alert('Error delete');
    },
  });

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  const handleEditDialog = () => {
    setPostId(post.id);
    openDialog(true);
  };

  const handleDeleteClick = (id: number) => {
    if (window.confirm('Are you sure to delete?')) {
      handleDeletePost(id);
    }
  };

  return (
    <div style={{ margin: '10px' }}>
      <Card sx={{ width: 350 }}>
        <CardHeader
          avatar={<Avatar sx={{ bgcolor: red[500] }} aria-label="recipe">U{post.id}</Avatar>}
          action={<IconButton aria-label="settings"><MoreVertIcon /></IconButton>}
          title="Doro Onome Churchill"
          subheader="nomzykush@gmail.com"
        />
        <CardContent>
          <Typography variant="body2" color="text.secondary">
            {post.title}
          </Typography>
        </CardContent>
        <CardActions disableSpacing>

          <IconButton onClick={handleEditDialog} aria-label="edit">
            <EditIcon />
          </IconButton>

          <IconButton onClick={() => handleDeleteClick(post.id)} aria-label="delete">
            <DeleteIcon />
          </IconButton>

          <ExpandMore
            expand={expanded}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="show more">
            <ExpandMoreIcon />
          </ExpandMore>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography paragraph>Description</Typography>
            <Typography paragraph>{post.body}</Typography>
          </CardContent>
        </Collapse>
      </Card>
    </div >
  );
}

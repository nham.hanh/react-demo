import * as React from 'react';
import { useQuery } from "react-query";
import { getAllPosts } from './api/restfulApi';
import PostCard from './PostCard'
import { Link, Grid } from '@mui/material';
import { PostResponse, GetsResponse } from './types/post';

export default function Posts({ onSetPostId: setPostId, onOpenDialog: openDialog, listPost, onSetListPost }: { onSetPostId: Function, onOpenDialog: Function, listPost: GetsResponse, onSetListPost: Function }) {

  const { isLoading, data, isError } = useQuery(['posts'], () => getAllPosts(), { enabled: true });
  React.useEffect(() => {
    onSetListPost(data!);
  }, ([data]));

  if (isLoading) {
    return "Loading...";
  }



  if (isError) {
    return "An error has occurred: "; // error message
  }

  return (
    <>
      <Grid container>
        {listPost && listPost.map((post: PostResponse) => (
          <PostCard key={post.id} post={post} onSetPostId={setPostId} onOpenDialog={openDialog} listPost={listPost} onSetListPost={onSetListPost}/>
        ))}
      </Grid>
      <Link color="primary" href="#" sx={{ mt: 3 }}>
        See more posts
      </Link>
    </>
  );
}
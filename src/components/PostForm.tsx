import { useState, useEffect } from 'react';
import { GetsResponse, PostRequest } from '../types/post';
import { useMutation, useQuery } from "react-query";
import { createPost, savePost, getPost } from '../api/restfulApi';
import {TextareaAutosize, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Button, TextField } from '@mui/material';

export default function PostForm({ open, openDialog: openDialogFn, postId, listPost, onSetListPost }: { open: boolean, openDialog: Function, postId: number, listPost: GetsResponse, onSetListPost: Function }) {
  // const [openDialog, setOpenDialog] = useState<boolean>(false);
  let openDialog = false;
  const [param, setFormData] = useState<PostRequest>({ "userId": 0, "title": '', "body": '' });
  const { isLoading, data: myPost } = useQuery(['post', postId], () => getPost(postId), { enabled: !!postId && open });

  useEffect(() => {
    setFormData({ "userId": myPost?.userId!, "title": myPost?.title!, "body": myPost?.body! });
  }, ([myPost]));

  if (!isLoading && open) {
    // setOpenDialog(true);
    openDialog = true;
  }

  const handleClose = () => {
    openDialogFn(false);
  };


  const { mutate: handleCreatePost } = useMutation((param: PostRequest) => createPost(param), {
    onSuccess(res) {
      alert("Success create post");
      openDialogFn(false);

      //get maxid , because mock test api always return id 101
      const ids = listPost.map(object => {
        return object.id;
      });
      const maxId = Math.max(...ids);
      res.id = maxId + 1;

      //set list state
      const tmpList = [...listPost];
      tmpList.unshift(res);
      onSetListPost(tmpList);
    },
    onError() {
      alert('Error create');
    },
  });

  const { mutate: handleSavePost } = useMutation(({ id, param }: { id: number; param: PostRequest }) => savePost({ id, body: param }), {
    onSuccess(res) {
      alert("Success save post");
      openDialogFn(false);

      const foundIndex = listPost.findIndex(obj => obj.id === res.id);
      console.log(foundIndex);
      const tmpList = [...listPost];
      if (foundIndex >= 0) {
        tmpList[foundIndex] = res;
        onSetListPost(tmpList);
      }
      else {
        console.log("Exception: not found post to edit")
      }

    },
    onError() {
      alert('Error save');
    },
  });

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (postId === 0) {
      handleCreatePost(param);
    }
    else {
      handleSavePost({ id: postId, param });
    }
  }

  return (
    <Dialog open={openDialog} onClose={handleClose}>
      <form onSubmit={handleSubmit}>
        <DialogTitle>Post Form</DialogTitle>
        <DialogContent>
          <DialogContentText>Please input the value below and click submit to save</DialogContentText>

          <TextField
            autoFocus
            margin="dense"
            id="title"
            name="title"
            label="Title"
            type="text"
            fullWidth
            onChange={e => setFormData({ ...param, ["title"]: e.target.value })}
            defaultValue={myPost?.title}
          />
          <TextareaAutosize
            style={{ width: "99%", height:"100px", border: "1px solid lightgray" }}
            autoFocus
            id="body"
            placeholder='Body'
            onChange={e => setFormData({ ...param, ["body"]: e.target.value })}
            defaultValue={myPost?.body}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">Cancel</Button>
          <Button type="submit" color="primary">Save</Button>
        </DialogActions>
      </form>
    </Dialog>
  )
}
import Posts from './Posts'
import { AppBar, Typography, Toolbar, IconButton } from '@mui/material';
import PostForm from './components/PostForm';
import { useState } from 'react';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import { GetsResponse } from './types/post';

export default function App() {

  const [isShow, setIsShow] = useState(false);
  const [postId, setPostId] = useState(0);
  const [listPost, setListPost] = useState<GetsResponse>([]);

  const addNewPost = () => {
    setPostId(0);
    setIsShow(true);
  }

  const handleSetPostId = (id: number) => {
    setPostId(id);
  }

  const handleOpenDialog = (show: boolean) => {
    setIsShow(show);
  }

  return (
    <div style={{ padding: 14 }} className="App">
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            Posts
          </Typography>
          <IconButton onClick={addNewPost}>
            <AddCircleOutlineIcon />
          </IconButton>
        </Toolbar>
      </AppBar>

      <Posts onSetPostId={handleSetPostId} onOpenDialog={handleOpenDialog} listPost={listPost} onSetListPost={setListPost} />
      <PostForm open={isShow} openDialog={setIsShow} postId={postId} listPost={listPost} onSetListPost={setListPost}></PostForm>
    </div>
  );
}
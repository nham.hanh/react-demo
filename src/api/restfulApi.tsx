import axios from 'axios';
import { GetsResponse, PostRequest, PostResponse } from '../types/post';

export const apiClient = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
});

export const getAllPosts = async () => {
    const response = await apiClient.get<GetsResponse>(`posts`);
    return response.data;
};

export const getPost = async (id: number) => {
    const response = await apiClient.get<PostResponse>(`posts/${id}`);
    return response.data;
};

export const createPost = async (body: PostRequest) => {
    const response = await apiClient.post<PostResponse>(`posts`, body);
    return response.data;
};

export const savePost = async ({ id, body }: { id: number; body: PostRequest; }) => {
    const response = await apiClient.put<PostResponse>(`posts/${id}`, body);
    return response.data;
};

export const deletePost = async (id: number) => {
    const response = await apiClient.delete(`posts/${id}`);
    return response.data;
};
